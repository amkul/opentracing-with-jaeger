import sys, logging
from jaeger_client import Config
import time

def init_tracer(service):
    logging.getLogger('').handlers = []
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)
    config = Config(
        config={
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            'logging': True,
        },
        service_name=service,
    )

    # this call also sets opentracing.tracer
    return config.initialize_tracer()

tracer = init_tracer('hello-world')

# def say_hello(name):
#     span = tracer.start_span('say-hello-operation')
#     msg = 'Hello, %s!' % name
#     print(msg)
#     span.finish()
def say_hello(name):
    with tracer.start_span('say-hello-operation') as span:
        span.set_tag('hello-to', name)
        msg = 'Hello, %s!' % name
        span.log_kv({'event': 'string-format', 'value': msg})
        print(msg)
        span.log_kv({'event': 'println'})

name = sys.argv[1]
say_hello(name)

# yield to IOLoop to flush the spans
time.sleep(2)
tracer.close()
