# OpenTracing with Jaeger

## Installing

Using CNCF Jaeger (https://github.com/jaegertracing/jaeger) as the tracing backend,
[see here](../README.md) how to install it in a Docker image.

This repository uses [virtualenv](https://pypi.python.org/pypi/virtualenv) and `pip` to manage dependencies.
To install all dependencies, run:

```
cd opentracing-with-jaeger
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```
